


from enum import Enum


class RFEM(Enum):
    '''
    Restrict find methods
    '''
    FIND = "r_find_ele"
    FINDS = "r_find_eles"
