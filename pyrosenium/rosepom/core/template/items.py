from pyrosenium.rosepom.helpers.rosenium_find_elements_methods import RFEM
from pyrosenium.rosepom.core import rosepom


class TPLTemplateLPTItems(rosepom.Items):
    '''
    Contains find methods and locators
    '''
    # EXAMPLE_ITEM = (RFEM.FIND, ["css", "div", 0])
